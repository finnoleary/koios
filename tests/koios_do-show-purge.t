Setup test environment

  $ cd $TESTDIR/../tmpmnt

Setup filesystem tree

  $ unset XDG_CONFIG_PATH
  $ unset HOME
  $ export HOME=.
  $ mkdir foo foo/bar foo/qux
  $ touch foo/a foo/b foo/c foo/bar/h foo/bar/i foo/bar/j

Setup koios system

  $ ./../koios init .
  $ ./../koios tags +dummy1 +dummy2
  $ ./../koios tags
  dummy1
  dummy2

Tag the tree

  $ ./../koios +dummy1 foo/qux foo/bar

Confirm that we tagged the tree

  $ ./../koios show foo
  foo/bar
  foo/bar/h
  foo/bar/i
  foo/bar/j
  foo/qux
  $ ./../koios show +dummy1 foo
  foo/bar
  foo/bar/h
  foo/bar/i
  foo/bar/j
  foo/qux

Tag some more files to exclude and confirm the tags

  $ ./../koios +dummy2 foo/bar/h foo/bar/j
  $ ./../koios show --tags +dummy1 foo
  foo/bar: dummy1 
  foo/bar/h: dummy2 dummy1 
  foo/bar/i: dummy1 
  foo/bar/j: dummy2 dummy1 
  foo/qux: dummy1 

Confirm koios show tag exclusion

  $ ./../koios show +dummy1 -dummy2 foo
  foo/bar
  foo/bar/i
  foo/qux

Test koios show --depth option

  $ ./../koios show --depth 1 +dummy1 foo
  foo/bar
  foo/qux

Test koios purge

  $ ./../koios purge foo/bar/*
  $ ./../koios show --tags +dummy1 foo
  foo/bar: dummy1 
  foo/qux: dummy1 

Test koios do remove

  $ ./../koios -C foo -dummy1 .
  $ ./../koios show +dummy1 foo

Cleanup

  $ rm -rf foo
  $ rm .koios.cfg
