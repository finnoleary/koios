
  $ cd $TESTDIR/../tmpmnt

Test koios tags no config failure

  $ ./../koios tags

Test koios tags fresh

  $ ./../koios init .
  $ ./../koios tags +dummya +dummyb
  $ ./../koios tags
  dummya
  dummyb

Test koios tags add remove

  $ ./../koios tags +dummyc -dummyb
  $ ./../koios tags
  dummya
  dummyc

Test koios tags different folder

  $ mkdir foo
  $ ./../koios init -C foo .
  $ ./../koios tags -C foo +dummyz +dummyy
  $ ./../koios tags -C foo
  dummyz
  dummyy

Cleanup!

  $ rm -rf foo
  $ rm .koios.cfg
