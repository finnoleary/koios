Koios user tutorial
v0.0
Finn Alexander O'Leary
2019-08-14

WHAT IS KOIOS?

Koios is a general purpose tagging system for your files! No longer are you
bounded by the traditional filesystem hierarchy. No longer do you have to
decide how to categorize your information.

WHAT MAKES IT DIFFERENT THAN ALL OF THE OTHER TAGGING SYSTEMS?

Koios uses something called 'extended attributes', this means that the tags
become an invisible part of how the file is stored. Instead on relying on a
database, which is fragile and can fatally break when you rename, overwrite,
or move the file. 


BUT YOU SAID NO DATABASES?

Ah, right. For compactness sake we store tags as 'bits', and store the names
of the tags in a file. This makes comparing tags, calculating subsets, and
renaming tags really efficient. It also means we can store a lot of tags in a
very small space. A typical ext4 file system installation uses a block size
of 1024 bytes, which allows you to store 8192 tags. Some Apple systems can
store (256KB - 1) of extended attributes which gives us 2047992 available tags.
Not bad! 


WHAT'S THE DOWNSIDE?

Because of janky non-standard filesystem support, moving tags across
file-systems might cause some or all tags to be lost. Unfortunately this is
unsolvable without patching a bunch of filesystem code!


OK, HOW DO I USE IT?

Well, first off. Koios has several names for things that you might find weird,
so I'll introduce them here:

A 'tag system' is a specific subdiectory (and all it's contents) of the
filesystem. It's it's own 'subspace' of tags. You have a default one, in your
home config folder, and then you can make more if you run out of tag names or
are on a different filesystem. It's basically like the .git/ directory, and
created in much the same way.

Creating a new tag system is as simple as doing

$ koios init

by default this will create a new tag system in your home folder, but if you
want to create one specific to the current directory you're in, you can do:

$ koios init .

You can also do

$ koios init -i <path-to-other-config> if you want to import the tag names used
in another tag system.

Anyway!

You can view the tags in the current tag system by doing:

$ koios tags

If you're trying this for the first time, it won't show anything, because you
have no tags! So let's go and add a few:

$ koios tags +meme +comic

Now let's see if koios has picked up our new tags:

$ koios tags
meme
comic

Cool! But this would be useless without tagging anything so let's go ahead and
do that:

$ koios +comic smbc_*.png

This helps make the interface uniform. Now, did it do
anything? Let's check! You can show all of the files tagged with "comic" by
using `koios show`, like so:

$ koios show +comic
smbc_games-for-humans.png
smbc_monty-hall-problems.png
...


Note that the same plus/minus syntax was used in creating tags is used in
adding them, too. This syntax ends up being quite flexible in representing
filters. For example, the following command lists all files tagged with 'comic'
and 'hawkeye', that don't include the tag 'spiderman':

$ koios show +comic +hawkeye -spiderman .

But tagging files by hand is boring, right? For that reason, koios integrates
with libmagic to help you tag your files automatically. To do this, you need
to know the rough mime type, which is usually easy to determine. For an
example, let's tag all the C files in the current directory tree:

$ koios auto text/x-c +cfile .

What if we decide to rename from cfile to c_file?

$ koios rename cfile c_file

Or, instead, let's remove some tags altogether from the tag system:

$ koios tags -c_file -comic

It's super important to note that that command doesn't automatically remove
'comic' and 'c_file' from the files, the bits will still be set in those files,
but they won't be mapped to anything. This is because koios a) doesn't store
the location of files, and b) it's both expensive and potentially very bad
behaviour for koios to scan everywhere and remove those set bits. So first it's
important to remove those tags. As we've only bothered to current directory
tree, we can remove them like so:

$ koios -c_file -comic .

Ok, I'm bored now. Let's make koios stop tagging our files entirely.

$ koios purge *


WHAT NOW?

That's about it! It's simple to integrate koios into your existing workflow,
and if you're a developer it comes with a library to play around with! You can
find more detailed information in the README or manual page.

